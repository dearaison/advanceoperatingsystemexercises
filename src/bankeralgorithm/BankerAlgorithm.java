package bankeralgorithm;

public class BankerAlgorithm {
	public String findSafeSequence(int[] available, int[][] max, int[][] allocation) {
		int numberOfProcess = max.length;
		int numberOfResourceType = max[0].length;
		String sesultSequence = "";

		int[][] need = new int[numberOfProcess][numberOfResourceType];
		for (int i = 0; i < need.length; i++) {
			for (int j = 0; j < need[i].length; j++) {
				need[i][j] = max[i][j] - allocation[i][j];
			}
		}

		int[] work = available;
		boolean[] finish = new boolean[numberOfProcess];

		for (int i = 0; i < finish.length; i++) {
			finish[i] = false;
		}

		boolean check = true;
		while (check) {
			check = false;
			for (int i = 0; i < numberOfProcess; i++) {
				if (!finish[i]) {
					int j;
					for (j = 0; j < numberOfResourceType; j++) {
						if (need[i][j] > work[j]) {
							break;
						}
					}
					if (j == numberOfResourceType) {
						for (j = 0; j < numberOfResourceType; j++) {
							work[j] = work[j] + allocation[i][j];
						}
						finish[i] = true;
						check = true;
						sesultSequence += "P" + i + ",";
					}
				}
			}
		}

		int i;
		for (i = 0; i < numberOfProcess; i++) {
			if (!finish[i])
				break;
		}

		if (i == numberOfProcess) {
			return sesultSequence;
		} else {
			return "DEAD LOCK";
		}
	}

	public void print(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		BankerAlgorithm test1 = new BankerAlgorithm();
		int[] a = { 3, 3, 2 };
		int[][] m = { { 7, 5, 3 }, { 9, 2, 2 }, { 5, 0, 2 }, { 2, 2, 2 }, { 4, 3, 3 } };
		int[][] all = { { 0, 1, 0 }, { 2, 0, 0 }, { 3, 0, 2 }, { 2, 1, 1 }, { 0, 0, 2 } };
		System.out.println(test1.findSafeSequence(a, m, all));

	}

}
