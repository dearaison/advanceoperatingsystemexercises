package cpuscheduling;

import java.applet.*;
import java.awt.*;
import java.io.*;

/*
<applet code="rectangle" height=500 width=700>
</applet>
*/

@SuppressWarnings({ "deprecation", "serial" })
public class CPUScheduling extends Applet {
	int WaitingTimeOfEachProcessResult[];
	int processes[];
	int i = 0, theNumberOfProcess, theNumberOfProcess2;
	Graphics g = getGraphics();

	public void init() {
		try {

			BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

			System.out.println("ENTER no. of processes:");
			theNumberOfProcess = Integer.parseInt(obj.readLine());

			int option;
			int burstTimes[] = new int[theNumberOfProcess];

			do {

				System.out.println("MENU FOR CPU SCHEDULING");
				System.out.println("1.FCFS method");
				System.out.println("2.SHORTEST JOB FIRST method(non-preemptive)");
				System.out.println("3.SHORTEST JOB FIRST method(preemptive)");
				System.out.println("4.ROUND ROBIN method");
				System.out.println("5.PRIORITY method (non-preemptive)");
				System.out.println("6.PRIORITY method (preemptive)");
				System.out.println("7.EXIT");
				System.out.println("ENTER YOUR CHOICE");

				option = Integer.parseInt(obj.readLine());

				switch (option) {
				case 1:

				{

					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("ENTER burst time for each process: p" + (i + 1));
						burstTimes[i] = Integer.parseInt(obj.readLine());
					}
					theNumberOfProcess2 = theNumberOfProcess;
					int turn[] = new int[theNumberOfProcess];
					WaitingTimeOfEachProcessResult = new int[theNumberOfProcess + 1];
					WaitingTimeOfEachProcessResult[0] = 0;
					float totalWaitingTime = 0;
					float totalTurnAroundTime = 0;
					processes = new int[theNumberOfProcess];
					for (i = 1; i <= theNumberOfProcess; i++)
						WaitingTimeOfEachProcessResult[i] = burstTimes[i - 1] + WaitingTimeOfEachProcessResult[i - 1];
					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("indivisual waiting time for process p" + (i + 1) + "is"
								+ WaitingTimeOfEachProcessResult[i] + " ");
					}

					for (i = 0; i < theNumberOfProcess; i++) {
						processes[i] = i + 1;
						System.out.print(processes[i]);
					}

					for (i = 0; i < theNumberOfProcess; i++)
						totalWaitingTime += WaitingTimeOfEachProcessResult[i];

					float averageWaitingTime = totalWaitingTime / theNumberOfProcess;
					System.out.println("average waiting time is:" + averageWaitingTime);

					/*
					 * for( i=0;i<n;i++) System.out.println("response for process p"+(i+1)+"is"+
					 * apw1[i]+" ");
					 */

					for (i = 0; i < theNumberOfProcess; i++) {
						turn[i] = burstTimes[i] + WaitingTimeOfEachProcessResult[i];
						System.out.println("turnaround time for process p" + (i + 1) + "is" + turn[i] + " ");
					}

					for (i = 0; i < theNumberOfProcess; i++)
						totalTurnAroundTime += turn[i];

					float averageTurnAroundTime = totalTurnAroundTime / theNumberOfProcess;
					System.out.println("average turn-around time is:" + averageTurnAroundTime);

					paint(g);
				}

					break;

				case 2: {
					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("ENTER burst time for each process: p" + (i + 1));
						burstTimes[i] = Integer.parseInt(obj.readLine());
					}
					theNumberOfProcess2 = theNumberOfProcess;
					int temp;
					float totalWaitingTime = 0;
					int turn[] = new int[theNumberOfProcess];
					WaitingTimeOfEachProcessResult = new int[theNumberOfProcess + 1];
					WaitingTimeOfEachProcessResult[0] = 0;
					processes = new int[theNumberOfProcess];
					int sortedJobQueue[] = new int[theNumberOfProcess];
					float totalTurnAroundTime = 0;

					for (i = 0; i < theNumberOfProcess; i++)
						sortedJobQueue[i] = burstTimes[i];

					for (i = 0; i < theNumberOfProcess; i++)
						for (int j = i + 1; j < theNumberOfProcess; j++)
							if (sortedJobQueue[i] > sortedJobQueue[j]) {
								temp = sortedJobQueue[i];
								sortedJobQueue[i] = sortedJobQueue[j];
								sortedJobQueue[j] = temp;
							}

					for (i = 0; i < theNumberOfProcess; i++)
						for (int j = 0; j < theNumberOfProcess; j++)
							if (sortedJobQueue[i] == burstTimes[j])
								processes[i] = j + 1;

					for (i = 0; i < theNumberOfProcess; i++)
						WaitingTimeOfEachProcessResult[i + 1] = sortedJobQueue[i] + WaitingTimeOfEachProcessResult[i];

					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("indivisual waiting time for process p" + processes[i] + "is"
								+ WaitingTimeOfEachProcessResult[i] + " ");
					}

					for (i = 0; i < theNumberOfProcess; i++)
						totalWaitingTime += WaitingTimeOfEachProcessResult[i];
					float avg = totalWaitingTime / theNumberOfProcess;
					System.out.println("average waiting time is:" + avg);

					/*
					 * for( i=0;i<n;i++) System.out.println("response for process p"+p[i]+"is"+
					 * apw1[i]+" ");
					 */

					for (i = 0; i < theNumberOfProcess; i++) {
						int k = processes[i];
						turn[i] = burstTimes[k - 1] + WaitingTimeOfEachProcessResult[i];
						System.out.println("turnaround time for process p" + processes[i] + "is" + turn[i] + " ");
					}

					for (i = 0; i < theNumberOfProcess; i++)
						totalTurnAroundTime += turn[i];

					float avg1 = totalTurnAroundTime / theNumberOfProcess;
					System.out.println("average turn-around time is:" + avg1);

					paint(g);

				}

					break;

				case 3: {

					int totalBustTime = 0, timePoint = 0, arrivedProcess = 0, small = 999, executingProcess = 0,
							positionOfExecutingProcess = 0, x = 0, count = 0;
					int pro[][] = new int[theNumberOfProcess][3];
					int waitingTimeOfEachProcess[] = new int[theNumberOfProcess];
					int wt1[] = new int[theNumberOfProcess];
					int bt1[] = new int[theNumberOfProcess];
					int tt[] = new int[theNumberOfProcess];
					WaitingTimeOfEachProcessResult = new int[20];
					processes = new int[20];

					for (int i = 0; i < theNumberOfProcess; i++) {
						pro[i][0] = i;
						System.out.print("Burst Time for P" + i + " :");
						pro[i][1] = Integer.parseInt(obj.readLine());
						totalBustTime += pro[i][1];
						System.out.print("Arrival Time for P" + i + " :");
						pro[i][2] = Integer.parseInt(obj.readLine());
					}
					for (int i = 0; i < theNumberOfProcess; i++)
						wt1[i] = pro[i][2];

					for (int i = 0; i < theNumberOfProcess; i++)
						bt1[i] = pro[i][1];

					for (int i = 0; i < theNumberOfProcess; i++)
						for (int j = i + 1; j < theNumberOfProcess; j++)
							if (pro[i][2] > pro[j][2]) {
								int temp[] = pro[i];
								pro[i] = pro[j];
								pro[j] = temp;
							}

					for (int i = 1; i <= totalBustTime; i++) {
						small = 999;
						for (int j = arrivedProcess; j < theNumberOfProcess; j++)
							if (timePoint >= pro[j][2])
								arrivedProcess++;
						for (int j = 0; j < arrivedProcess; j++) {
							if (small > pro[j][1] && pro[j][1] != 0) {
								small = pro[j][1];
								executingProcess = pro[j][0];
								positionOfExecutingProcess = j;
							}
						}
						if (processes[x] == executingProcess) {
							WaitingTimeOfEachProcessResult[x + 1]++;

						} else {
							x++;
							processes[x] = executingProcess;
							WaitingTimeOfEachProcessResult[x + 1] = WaitingTimeOfEachProcessResult[x];
							WaitingTimeOfEachProcessResult[x + 1]++;
							count++;
						}

						pro[positionOfExecutingProcess][1]--;
						if (pro[positionOfExecutingProcess][1] == 0)
							tt[positionOfExecutingProcess] = i;
						for (int j = 0; j < theNumberOfProcess; j++) {
							if (pro[j][1] != 0 && j != executingProcess)
								waitingTimeOfEachProcess[j]++;
						}
						timePoint++;
					}
					for (int i = 0; i < theNumberOfProcess; i++)
						for (int j = i + 1; j < theNumberOfProcess; j++)
							if (pro[i][0] > pro[j][0]) {
								int temp[] = pro[i];
								pro[i] = pro[j];
								pro[j] = temp;
								int tem = waitingTimeOfEachProcess[i], tem1 = tt[i];
								waitingTimeOfEachProcess[i] = waitingTimeOfEachProcess[j];
								waitingTimeOfEachProcess[j] = tem;
								tt[i] = tt[j];
								tt[j] = tem1;

							}

					System.out.println();

					for (int m = 0; m <= count; m++)
						processes[m] += 1;

					double awt = 0.0, att = 0.0;

					System.out.println();
					for (int i = 0; i < theNumberOfProcess; i++) {
						System.out.println(
								"waiting time for process p " + i + "  " + (waitingTimeOfEachProcess[i] - wt1[i]));
						awt += (waitingTimeOfEachProcess[i] - wt1[i]);
						att += (waitingTimeOfEachProcess[i] - wt1[i] + bt1[i]);
					}
					for (int i = 0; i < theNumberOfProcess; i++) {
						System.out.println("turnaround time for process p" + i + "  "
								+ (waitingTimeOfEachProcess[i] - wt1[i] + bt1[i]));
					}
					System.out.println("Average waiting time : " + (awt / theNumberOfProcess));
					System.out.println("Average turnaround time : " + (att / theNumberOfProcess));
					theNumberOfProcess2 = count + 1;
					paint(g);

				}
					break;

				case 4: {
					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("ENTER burst time for each process: p" + (i + 1));
						burstTimes[i] = Integer.parseInt(obj.readLine());
					}
					int ro[] = new int[theNumberOfProcess];
					WaitingTimeOfEachProcessResult = new int[20];
					WaitingTimeOfEachProcessResult[0] = 0;

					int sq[] = new int[20];

					int temp;
					float t = 0;
					float tu = 0;
					int apw[] = new int[theNumberOfProcess];
					@SuppressWarnings("unused")
					int bttime = 0;
					int turn[] = new int[theNumberOfProcess];
					int f = 0;

					for (i = 0; i < theNumberOfProcess; i++) {
						ro[i] = burstTimes[i];

					}

					processes = new int[30];
					System.out.println("enter time quantum");
					int ts = Integer.parseInt(obj.readLine());
					int k = 0;
					sq[k] = 0;
					i = 0;
					temp = ts;

					int count = 0;

					do {

						if (ro[i] == 0) {
							i++;
							sq[k] = sq[k - 1] + temp;
						}

						else if (ro[i] >= ts) {
							ro[i] = ro[i] - ts;
							processes[k] = i + 1;
							k++;
							i++;
							temp = ts;
							sq[k] = sq[k - 1] + temp;
							turn[i - 1] = sq[k];

							f = 0;
							for (int m = 0; m < theNumberOfProcess; m++)
								f += ro[m];

							count++;
						}

						else if (ro[i] > 0 && ro[i] < ts) {
							temp = ro[i];
							ro[i] = 0;
							processes[k] = i + 1;
							i++;
							k++;
							sq[k] = sq[k - 1] + temp;
							turn[i - 1] = sq[k];

							f = 0;
							for (int m = 0; m < theNumberOfProcess; m++)
								f += ro[m];

							count++;

						}

						if (i == theNumberOfProcess)
							i = 0;

					} while (f != 0);

					System.out.println();

					for (i = 0; i <= count; i++) {
						WaitingTimeOfEachProcessResult[i] = sq[i];
					}

					System.out.println();

					for (i = 0; i < theNumberOfProcess; i++) {
						apw[i] = turn[i] - burstTimes[i];
						t += apw[i];
						tu += turn[i];
					}

					for (i = 0; i < theNumberOfProcess; i++)
						System.out.println(" waiting time for p" + (i + 1) + "is:" + apw[i]);

					float avg = t / theNumberOfProcess;
					System.out.println("average waiting time is:" + avg);

					for (i = 0; i < theNumberOfProcess; i++)
						System.out.println("turn around time is:" + turn[i]);

					float avg1 = tu / theNumberOfProcess;
					System.out.println("average turn-around time is:" + avg1);

					theNumberOfProcess2 = count;

					paint(g);

				}
					break;

				case 5: {
					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("ENTER burst time for each process: p" + (i + 1));
						burstTimes[i] = Integer.parseInt(obj.readLine());
					}
					theNumberOfProcess2 = theNumberOfProcess;
					int pr[] = new int[theNumberOfProcess];
					int pr1[] = new int[theNumberOfProcess];
					float t = 0;
					int temp;
					int turn[] = new int[theNumberOfProcess];
					processes = new int[theNumberOfProcess];
					WaitingTimeOfEachProcessResult = new int[theNumberOfProcess + 1];
					WaitingTimeOfEachProcessResult[0] = 0;
					float tu = 0;

					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("Enter the priority for p" + (i + 1));
						pr[i] = Integer.parseInt(obj.readLine());
					}

					for (i = 0; i < theNumberOfProcess; i++) {
						pr1[i] = pr[i];
					}

					for (i = 0; i < theNumberOfProcess; i++)
						for (int j = i + 1; j < theNumberOfProcess; j++)
							if (pr1[i] > pr1[j]) {
								temp = pr1[i];
								pr1[i] = pr1[j];
								pr1[j] = temp;
							}

					for (i = 0; i < theNumberOfProcess; i++)
						for (int j = 0; j < theNumberOfProcess; j++)
							if (pr1[i] == pr[j])
								processes[i] = j + 1;

					for (i = 0; i < theNumberOfProcess; i++) {
						int k = processes[i];
						WaitingTimeOfEachProcessResult[i + 1] = burstTimes[k - 1] + WaitingTimeOfEachProcessResult[i];
					}

					for (i = 0; i < theNumberOfProcess; i++) {
						System.out.println("indivisual waiting time for process p" + processes[i] + "is"
								+ WaitingTimeOfEachProcessResult[i] + " ");
					}

					for (i = 0; i < theNumberOfProcess; i++)
						t += WaitingTimeOfEachProcessResult[i];
					float avg = t / theNumberOfProcess;
					System.out.println("average waiting time is:" + avg);

					/*
					 * for( i=0;i<n;i++) System.out.println("response for process p"+p[i]+"is"+
					 * apw1[i]+" ");
					 */

					for (i = 0; i < theNumberOfProcess; i++) {
						int k = processes[i];
						turn[i] = burstTimes[k - 1] + WaitingTimeOfEachProcessResult[i];
						System.out.println("turnaround time for process p" + processes[i] + "is" + turn[i] + " ");
					}

					for (i = 0; i < theNumberOfProcess; i++)
						tu += turn[i];

					float avg1 = tu / theNumberOfProcess;
					System.out.println("average turn-around time is:" + avg1);

					paint(g);

				}

					break;

				case 6:

				{
					final int NAME = 0;
					final int PRIORITY = 1;
					final int ARRIVAL_TIME = 2;
					final int BURST_TIME = 3;

					int totalBurstTime = 0, timePoint = 0, arrivedProcess = 0, small = 999, executingProcess = 0,
							positionOfExecutingProcess = 0, x = 0, count = 0;
					int pro[][] = new int[theNumberOfProcess][4];
					int waitingTimeOfEachProcess[] = new int[theNumberOfProcess];
					int arrivalTimeOfEachProcess[] = new int[theNumberOfProcess];
					int burstTimeOfEachProcess[] = new int[theNumberOfProcess];
					int terminatedTimePoint[] = new int[theNumberOfProcess];
					WaitingTimeOfEachProcessResult = new int[20];
					processes = new int[20];

					for (int i = 0; i < theNumberOfProcess; i++) {
						pro[i][NAME] = i;

						System.out.print("Priority for P" + i + " :");
						pro[i][PRIORITY] = Integer.parseInt(obj.readLine());

						System.out.print("Arrival Time for P" + i + " :");
						pro[i][2] = Integer.parseInt(obj.readLine());

						System.out.print("Burst Time for P" + i + " :");
						pro[i][3] = Integer.parseInt(obj.readLine());
						totalBurstTime += pro[i][3];
					}
					for (int i = 0; i < theNumberOfProcess; i++)
						arrivalTimeOfEachProcess[i] = pro[i][ARRIVAL_TIME];

					for (int i = 0; i < theNumberOfProcess; i++)
						burstTimeOfEachProcess[i] = pro[i][BURST_TIME];

					for (int i = 0; i < theNumberOfProcess; i++)
						for (int j = i + 1; j < theNumberOfProcess; j++)
							if (pro[i][ARRIVAL_TIME] > pro[j][ARRIVAL_TIME]) {
								int temp[] = pro[i];
								pro[i] = pro[j];
								pro[j] = temp;
							}

					for (int i = 1; i <= totalBurstTime; i++) {
						small = 999;
						for (int j = arrivedProcess; j < theNumberOfProcess; j++)
							if (timePoint >= pro[j][ARRIVAL_TIME])
								arrivedProcess++;
						for (int j = 0; j < arrivedProcess; j++) {
							if (small > pro[j][PRIORITY] && pro[j][BURST_TIME] != 0) {
								small = pro[j][PRIORITY];
								executingProcess = pro[j][NAME];
								positionOfExecutingProcess = j;
							}
						}
						if (processes[x] == executingProcess) {
							WaitingTimeOfEachProcessResult[x + 1]++;

						} else {
							x++;
							processes[x] = executingProcess;
							WaitingTimeOfEachProcessResult[x + 1] = WaitingTimeOfEachProcessResult[x];
							WaitingTimeOfEachProcessResult[x + 1]++;
							count++;
						}

						pro[positionOfExecutingProcess][BURST_TIME] -= 1;
						if (pro[positionOfExecutingProcess][BURST_TIME] == 0)
							terminatedTimePoint[positionOfExecutingProcess] = i;
						for (int j = 0; j < theNumberOfProcess; j++) {
							if (pro[j][BURST_TIME] != 0 && j != executingProcess)
								waitingTimeOfEachProcess[j] += 1;
						}
						timePoint++;
					}
					for (int i = 0; i < theNumberOfProcess; i++)
						for (int j = i + 1; j < theNumberOfProcess; j++)
							if (pro[i][NAME] > pro[j][NAME]) {
								int temp[] = pro[i];
								pro[i] = pro[j];
								pro[j] = temp;
								int tem = waitingTimeOfEachProcess[i], tem1 = terminatedTimePoint[i];
								waitingTimeOfEachProcess[i] = waitingTimeOfEachProcess[j];
								waitingTimeOfEachProcess[j] = tem;
								terminatedTimePoint[i] = terminatedTimePoint[j];
								terminatedTimePoint[j] = tem1;

							}

					System.out.println();

					for (int m = 0; m <= count; m++)
						processes[m] += 1;

					double totalWaitingTime = 0.0, totalTurnAroundTime = 0.0;

					System.out.println();
					for (int i = 0; i < theNumberOfProcess; i++) {
						System.out.println("waiting time for process p " + i + "  "
								+ (waitingTimeOfEachProcess[i] - arrivalTimeOfEachProcess[i]));
						totalWaitingTime += (waitingTimeOfEachProcess[i] - arrivalTimeOfEachProcess[i]);
						totalTurnAroundTime += (waitingTimeOfEachProcess[i] - arrivalTimeOfEachProcess[i]
								+ burstTimeOfEachProcess[i]);
					}
					for (int i = 0; i < theNumberOfProcess; i++) {
						System.out.println("turnaround time for process p" + i + "  " + (waitingTimeOfEachProcess[i]
								- arrivalTimeOfEachProcess[i] + burstTimeOfEachProcess[i]));
					}
					System.out.println("Average waiting time : " + (totalWaitingTime / theNumberOfProcess));
					System.out.println("Average turnaround time : " + (totalTurnAroundTime / theNumberOfProcess));
					theNumberOfProcess2 = count + 1;

					paint(g);

				}

				}

			} while (option != 7);

		}

		catch (Exception e) {
		}

	}

	public void paint(Graphics g) {

		for (int j = 1; j <= theNumberOfProcess2; j++) {

			g.drawRect(50, 50, (WaitingTimeOfEachProcessResult[j] * 20), 30);
			g.drawString("p" + processes[j - 1], (55 + (WaitingTimeOfEachProcessResult[j - 1] * 20)), 70);
			g.drawString("" + WaitingTimeOfEachProcessResult[j - 1], 50 + (WaitingTimeOfEachProcessResult[j - 1] * 20),
					100);
		}
		g.drawString("" + WaitingTimeOfEachProcessResult[theNumberOfProcess2],
				50 + (WaitingTimeOfEachProcessResult[theNumberOfProcess2] * 20), 100);

	}

}