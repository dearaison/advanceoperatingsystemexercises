package multithreadexample;

import java.awt.Graphics;

public class Ball {
	int x, y, dx, dy;
//	Color color;

	public Ball(int x, int y/*, Color color*/) {
		super();
		this.x = x;
		this.y = y;
		this.dx = 1;
		this.dy = 1;
//		this.color = color;
	}

	public void move() {
		/*
		 * This method will help ball move if it reach to window edge the ball
		 * will turn back
		 */
		if (this.x + this.dx > 800 - 30) {
			this.dx = -1;
		} else if (this.x + this.dx < 0) {
			this.dx = 1;
		} else if (this.y + this.dy > 600 - 30) {
			this.dy = -1;
		} else if (this.y + this.dy < 0) {
			this.dy = 1;
		}
		this.x = this.x + this.dx;
		this.y = this.y + this.dy;
	}

	public Circle getBounds() {
		/* This method help you get a circle with contain the ball */
		return new Circle(this.x, this.y);
	}

	public boolean hasCollision(Ball that) {
		/*
		 * This method will decide which the ball collide another ball
		 * 
		 * A(xA, yA); B(xB, yB).
		 * 
		 * C1(Center A, radius R1); C1(Center B, radius R2).
		 * 
		 * distance between A and B: d = root(square(xA - xB) + square(yA - yB))
		 * 
		 * if d <= R1 + R2, C1 collide C2;
		 */
		double distanceBetweenTwoPoint = Math.sqrt(Math.pow(this.getBounds().x - that.getBounds().x, 2)
				+ Math.pow(this.getBounds().y - that.getBounds().y, 2));
		return distanceBetweenTwoPoint <= this.getBounds().radius + that.getBounds().radius;
	}

	public void handleCollision(Ball that) {
		if (this.hasCollision(that)) {
			this.dx = this.dx * (-1);
			this.dy = this.dy * (-1);
			that.dx = that.dx * (-1);
			that.dy = that.dy * (-1);
		}
	}

	public void paint(Graphics g) {
		/* This method draw the ball */
		g.fillOval(x, y, 30, 30);
	}
}
