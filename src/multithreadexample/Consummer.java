package multithreadexample;

public class Consummer implements Runnable {
	int waitingTime, numberOfProduct;

	public Consummer(int waitingTime, int numberOfProduct) {
		super();
		this.waitingTime = waitingTime;
		this.numberOfProduct = numberOfProduct;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < numberOfProduct; i++) {
				System.out.println("Consume: " + i);
				WareHouse.HOUSE.removeFirst();
				Thread.sleep(waitingTime);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.toString());
		}
	}
}
