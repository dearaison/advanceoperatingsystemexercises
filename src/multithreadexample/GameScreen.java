package multithreadexample;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class GameScreen extends JPanel implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Ball black, blue, red, cyan, green, pink;
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;

	static List<Ball> balls = new ArrayList<>();

	public GameScreen() {
		setPreferredSize(new Dimension(WIDTH, HEIGHT));

		black = new Ball(new Random().nextInt(600), new Random().nextInt(600));
		blue = new Ball(new Random().nextInt(600), new Random().nextInt(600));
		red = new Ball(new Random().nextInt(600), new Random().nextInt(600));
		cyan = new Ball(new Random().nextInt(600), new Random().nextInt(600));
		green = new Ball(new Random().nextInt(600), new Random().nextInt(600));
		pink = new Ball(new Random().nextInt(600), new Random().nextInt(600));

		// balls.add(black);
		// balls.add(blue);
		// balls.add(red);
		// balls.add(cyan);
		// balls.add(green);
		// balls.add(pink);
	}

	public void move() {
		black.move();
		blue.move();
		red.move();
		cyan.move();
		green.move();
		pink.move();

		// for (Ball ball : balls) {
		// ball.move();
		// }
	}

	public void handleConllision() {
		black.handleCollision(blue);
		black.handleCollision(red);
		black.handleCollision(cyan);
		black.handleCollision(green);
		black.handleCollision(pink);

		blue.handleCollision(red);
		blue.handleCollision(cyan);
		blue.handleCollision(green);
		blue.handleCollision(pink);

		red.handleCollision(cyan);
		red.handleCollision(green);
		red.handleCollision(pink);

		cyan.handleCollision(green);
		cyan.handleCollision(pink);

		green.handleCollision(pink);

		// for (int i = 0; i < balls.size() - 1; i++) {
		// balls.get(i).handleCollision(balls.get(i + 1));
		// }

	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.gray);
		g.fillRect(0, 0, 800, 6000);
		g.setColor(Color.black);
		black.paint(g);
		g.setColor(Color.blue);
		blue.paint(g);
		g.setColor(Color.RED);
		red.paint(g);
		g.setColor(Color.cyan);
		cyan.paint(g);
		g.setColor(Color.green);
		green.paint(g);
		g.setColor(Color.pink);
		pink.paint(g);

		// for (Ball ball : balls) {
		// g.setColor(ball.color);
		// ball.paint(g);
		// }
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (true) {
			this.handleConllision();
			repaint();
			this.move();
			try {
				Thread.sleep(3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {
		JFrame jf = new JFrame();
		jf.setVisible(true);
		jf.setResizable(false);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(800, 620);
		// jf.addMouseListener(new MouseListener() {
		//
		// @Override
		// public void mouseReleased(MouseEvent arg0) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void mousePressed(MouseEvent arg0) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void mouseExited(MouseEvent arg0) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void mouseEntered(MouseEvent arg0) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void mouseClicked(MouseEvent arg0) {
		// // TODO Auto-generated method stub
		// int red = new Random().nextInt(256);
		// int green = new Random().nextInt(256);
		// int blue = new Random().nextInt(256);
		// Color color = new Color(red, green, blue);
		// balls.add(new Ball(new Random().nextInt(600), new
		// Random().nextInt(600), color));
		//
		// }
		// });

		Container contain = jf.getContentPane();
		GameScreen gScreen = new GameScreen();
		contain.add(gScreen);
		jf.setLocationRelativeTo(null);

		Thread thread1 = new Thread(gScreen);
		thread1.start();
	}

}
