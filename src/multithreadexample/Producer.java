package multithreadexample;

public class Producer implements Runnable {
	int waitingTime, numberOfProduct;

	public Producer(int waitingTime, int numberOfProduct) {
		super();
		this.waitingTime = waitingTime;
		this.numberOfProduct = numberOfProduct;
	}

	@Override
	public void run() {
		try {
			for (int i = 0; i < numberOfProduct; i++) {
				WareHouse.HOUSE.add(i);
				System.out.println("Produce: " + i);
				Thread.sleep(waitingTime);
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.toString());
		}
	}
}
